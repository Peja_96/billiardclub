﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub
{
    class NoSuchPropertyException : Exception
    {
        public NoSuchPropertyException(string message) : base(message) { }
    }

}
