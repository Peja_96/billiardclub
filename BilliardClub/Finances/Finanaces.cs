﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub
{
    public class Finanaces
    {
        private decimal incomes;
        private decimal outcomes;
        

        public decimal Outcomes { get => outcomes; set => outcomes = value; }
        public decimal Incomes { get => incomes; set => incomes = value; }

        public decimal GetIncomesByType(string str)
        {
            decimal temp = 0;
            List<PlayingUnit> playingUnits = new PlayingUnitDAO().GetPlayingUnitsByType(str);
            playingUnits.ForEach(p =>
            {
                temp += p.Value;
            });
            return temp;
        }

        public decimal GetIncomesByTypeDate(string str,DateTime start, DateTime end)
        {
            decimal temp = 0;
            List<PlayingBill> bills = new PlayingBillDAO().GetBills(start,end);
            foreach(PlayingBill bill in bills)
            bill.PlayingUnits.ForEach(p =>
            {
                if(p.PlayingFieldGameType.Equals(str))
                temp += p.Value;
            });
            return temp;
        }

        public decimal GetSalaries()
        {
            decimal temp = 0;
            List<Employee> employees = new EmployeeDAO().GetEmployees();
            employees.ForEach(p =>
            {
                temp += p.Salary;
            });
            return temp;
        }
        public decimal GetFinancialEntries()
        {
            decimal temp = 0;
            List<FinancialEntry> entries = new FinancialEntryDAO().GetFinancialEntries();
            entries.ForEach(p =>
            {
                temp += p.Amount;
            });
            return temp;
        }

        public decimal GetFinancialEntriesByDate(DateTime start, DateTime end)
        {
            decimal temp = 0;
            
            List<FinancialEntry> entries = new FinancialEntryDAO().GetFinancialEntriesByDate(start, end);
            entries.ForEach(p =>
            {
                temp += p.Amount;
            });
            return temp;
        }
    }
}
