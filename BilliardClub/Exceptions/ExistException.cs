﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub
{
    class ExistException : Exception
    {
        
        public ExistException(string str) : base(str)
        {

        }
    }
}
