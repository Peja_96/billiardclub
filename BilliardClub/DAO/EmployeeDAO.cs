﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class EmployeeDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Employee> GetEmployees()
        {
            List<Employee> employees = new List<Employee>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from employee inner join person on id = personid where IsActive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    Salary = reader.GetDecimal("Salary")
                });
            }
            reader.Close();

            return employees;
        }
        public List<Employee> GetEmployeesByName(string name)
        {
            List<Employee> employees = new List<Employee>();
            MySqlCommand mySqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            mySqlCommand.CommandText = "select * from employee inner join person on id = personid where concat(Name, ' ', Surname) like @NameParam and isactive=1   ";
            mySqlCommand.Parameters.AddWithValue("@NameParam", new Regex("%" + name + "%"));
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            while (reader.Read())
            {
                employees.Add(new Employee()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    Salary = reader.GetDecimal("Salary")
                });
            }
            reader.Close();

            return employees;

        }
        public void AddEmployee(Employee employee)
        {
            new PersonDAO().AddPerson(employee);
            MySqlCommand mySqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            mySqlCommand.CommandText = "insert into employee(PersonID,ClubID,employmentRecordBook,Salary) values(@id,@clubID,@employmentRecordBook,@salary);";
            MySqlParameter id = new MySqlParameter() { ParameterName = "@id", Value = employee.ID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter clubID = new MySqlParameter(){ ParameterName = "@clubID", Value = Club.BilliardClub.ID, MySqlDbType = MySqlDbType.Int32};
            MySqlParameter employmentRecordBook = new MySqlParameter() { ParameterName = "@employmentRecordBook", Value = employee.EmploymentRecordBook, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter salary = new MySqlParameter() { ParameterName = "@salary", Value = employee.Salary, MySqlDbType = MySqlDbType.Decimal };
            mySqlCommand.Parameters.AddRange(new object[] { id, clubID, employmentRecordBook, salary });
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public void RemoveEmployee(Employee employee)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update person set isactive = 0 where id = @ID";
            command.Parameters.AddWithValue("@ID", employee.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public void UpdateEmployeeInfo(Employee newEmployee)
        {

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "UPDATE employee SET EmploymentRecordBook = @book, Salary = @salary WHERE PersonID = @id;";
            command.Parameters.AddWithValue("@book", newEmployee.EmploymentRecordBook);
            command.Parameters.AddWithValue("@salary", newEmployee.Salary);
            command.Parameters.AddWithValue("@id", newEmployee.ID);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
            command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "UPDATE person SET Name = @name, Surname = @surname, DateOfBirth = @Date WHERE ID = @id";
            command.Parameters.AddWithValue("@name", newEmployee.Name);
            command.Parameters.AddWithValue("@surname", newEmployee.Surname);
            command.Parameters.AddWithValue("@Date", newEmployee.DateOfBirth);
            command.Parameters.AddWithValue("@id", newEmployee.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }

        }
    }
}
