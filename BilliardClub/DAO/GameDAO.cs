﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class GameDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Game> GetGames()
        {
            List<Game> games = new List<Game>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from game";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                games.Add(new Game()
                {
                    ClubID = reader.GetInt32("ClubID"),
                    PricePerOur = reader.GetDecimal("PricePerOur"),
                    Type = reader.GetString("Type")
                });
            }
            reader.Close();
            return games;
        }

        public void ModifyGamePrice(Game game)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update game set PricePerOur = @pricePerOur where Type = @type";
            command.Parameters.AddWithValue("@pricePerOur", game.PricePerOur);
            command.Parameters.AddWithValue("@type", game.Type);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }catch(MySqlException ex)
            {
                log.Warn(ex);
            }
        }
    }
}
