﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class TelephoneDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddTelephone(string telephone)
        {
            if (IsDeactivatedTelephone(telephone))
                UpdateTelephone(telephone);
            else if(!IsActiveTelephone(telephone))
            {
                MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
                command.CommandText = "insert into telephone(number, clubid) values(@number, @id)";
                MySqlParameter number = new MySqlParameter() { ParameterName = "@number", Value = Int64.Parse(telephone), MySqlDbType = MySqlDbType.Int64 };
                MySqlParameter id = new MySqlParameter() { ParameterName = "@id", Value = Club.BilliardClub.ID, MySqlDbType = MySqlDbType.Int32 };
                command.Parameters.AddRange(new object[] { number, id });
                command.Prepare();
                try
                {
                    command.ExecuteNonQuery();
                }
                catch (MySqlException ex)
                {
                    log.Warn(ex);
                }
            }
        }

        public List<string> GetTelephones()
        {
            List<string> telephones = new List<string>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from telephone where isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                telephones.Add(reader.GetString("Number"));
            }
            reader.Close();

            return telephones;
        }

        public void RemoveTelephone(string number)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update telephone set isactive = 0 where number = @number";
            command.Parameters.AddWithValue("@number", number);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public bool IsDeactivatedTelephone(string number)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from telephone where number = @number and isactive = 0";
            command.Parameters.AddWithValue("@number", number);
            
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool IsActiveTelephone(string number)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from telephone where number = @number and isactive = 1";
            command.Parameters.AddWithValue("@number", number);

            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public void UpdateTelephone(string number)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update telephone set isactive = 1 where number = @number";
            command.Parameters.AddWithValue("@number", number);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }


    }
}
