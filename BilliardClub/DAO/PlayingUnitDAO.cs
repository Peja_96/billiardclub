﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class PlayingUnitDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<PlayingUnit> GetPlayingUnitsByType(string type)
        {
            List<PlayingUnit> playingUnits = new List<PlayingUnit>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingunit where PlayingFieldGameType= '" + type + "'";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingUnits.Add(new PlayingUnit()
                {
                    PlayingBillID = reader.GetInt32("PlayingBillID"),
                    PlayingFieldGameType = reader.GetString("PlayingFieldGameType"),
                    PlayingFieldPosition = reader.GetInt32("PlayingFieldPosition"),
                    TimePlayed = reader.GetTimeSpan("TimePlayed"),
                    Value = reader.GetDecimal("Value")
                });
            }
            reader.Close();
            return playingUnits;
        }

        public List<PlayingUnit> GetByID(int id)
        {
            List<PlayingUnit> playingUnits = new List<PlayingUnit>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingunit where PlayingBillID= '" + id + "'";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingUnits.Add(new PlayingUnit()
                {
                    PlayingBillID = reader.GetInt32("PlayingBillID"),
                    PlayingFieldGameType = reader.GetString("PlayingFieldGameType"),
                    PlayingFieldPosition = reader.GetInt32("PlayingFieldPosition"),
                    TimePlayed = reader.GetTimeSpan("TimePlayed"),
                    Value = reader.GetDecimal("Value")
                });
            }
            reader.Close();
            return playingUnits;
        }

        public void AddPlayingUnit(PlayingUnit unit)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into playingunit(TimePlayed,Value,PlayingBillID,PlayingFieldGameType,PlayingFieldPosition) values(@time, @value, @billid,@type,@position)";
            MySqlParameter time = new MySqlParameter() { ParameterName = "@time", Value = unit.TimePlayed, MySqlDbType = MySqlDbType.Time };
            MySqlParameter value = new MySqlParameter() { ParameterName = "@value", Value = unit.Value, MySqlDbType = MySqlDbType.Decimal };
            MySqlParameter billid = new MySqlParameter() { ParameterName = "@billid", Value = unit.PlayingBillID, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter type = new MySqlParameter() { ParameterName = "@type", Value = unit.PlayingFieldGameType, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter position = new MySqlParameter() { ParameterName = "@position", Value = unit.PlayingFieldPosition, MySqlDbType = MySqlDbType.Int32 };
            command.Parameters.AddRange(new object[] { time, value, billid, type,position });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
    }
}
