﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class PlayingFieldDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<PlayingField> GetPlayingFields()
        {
            List<PlayingField> playingFields = new List<PlayingField>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingfield where isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingFields.Add(new PlayingField()
                {
                    Position = reader.GetInt32("Position"),
                    GameType = reader.GetString("GameType"),
                    
                });
            }
            reader.Close();

            return playingFields;
        }
        public List<PlayingField> GetPlayingFields(string str)
        {
            List<PlayingField> playingFields = new List<PlayingField>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingfield where GameType="+"'"+str+"' and isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingFields.Add(new PlayingField()
                {
                    Position = reader.GetInt32("Position"),
                    GameType = reader.GetString("GameType")

                });
            }
            reader.Close();

            return playingFields;
        }

        public List<PlayingField> GetPlayingFieldsWithReservation(string str)
        {
            List<PlayingField> playingFields = new List<PlayingField>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingfield where GameType=" + "'" + str + "' and isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingFields.Add(new PlayingField()
                {
                    Position = reader.GetInt32("Position"),
                    GameType = reader.GetString("GameType"),
                    StopwatchTime = reader.GetDateTime("StopwatchTime"),
                    IsActive = reader.GetBoolean("IsActiveStopwatch")
                });
            }
            reader.Close();
            foreach(PlayingField field in playingFields)
            {
                field.Reservation = new ReservationDAO().GetReservation(field);
            }
            return playingFields;
        }
        public List<PlayingField> GetPlayingFieldsByType(string type)
        {
            List<PlayingField> playingFields = new List<PlayingField>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingfield where gametype like @TypeParam and isactive = 1";
            command.Parameters.AddWithValue("TypeParam", new Regex("%" + type + "%"));

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                playingFields.Add(new PlayingField()
                {
                    Position = reader.GetInt32("Position"),
                    GameType = reader.GetString("GameType")

                });
            }
            reader.Close();

            return playingFields;
        }

        public void AddField(PlayingField field)
        {
            
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into playingfield(Position,GameType,StopwatchTime) values(@position, @gameType, @stopwatchTime)";
            MySqlParameter position = new MySqlParameter() { ParameterName = "@position", Value = field.Position, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter gameType = new MySqlParameter() { ParameterName = "@gameType", Value = field.GameType, MySqlDbType = MySqlDbType.Enum };
            MySqlParameter stopwatchTime = new MySqlParameter() { ParameterName = "@stopwatchTime", Value = DateTime.Now, MySqlDbType = MySqlDbType.DateTime };
            command.Parameters.AddRange(new object[] { position, gameType,stopwatchTime });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }catch(MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public void RemoveField(PlayingField field)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update playingfield set isactive = 0 where position = @position and gametype = @gametype";
            command.Parameters.AddWithValue("@position", field.Position);
            command.Parameters.AddWithValue("@gametype", field.GameType);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public bool IsDeactivatedField(PlayingField field)
        {

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingfield where position = @position and gametype = @gametype and isactive = 0";
            command.Parameters.AddWithValue("@position", field.Position);
            command.Parameters.AddWithValue("@gametype", field.GameType);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public void DeactivateStopwatch(PlayingField field)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update playingfield set isactivestopwatch = 0 where position = @position and gametype = @gametype";
            command.Parameters.AddWithValue("@position", field.Position);
            command.Parameters.AddWithValue("@gametype", field.GameType);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void UpdateField(PlayingField field)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update playingfield set isactive = 1 where position = @position and gametype = @gametype";
            command.Parameters.AddWithValue("@position", field.Position);
            command.Parameters.AddWithValue("@gametype", field.GameType);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

        public void UpdateStopwatch(PlayingField field)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update playingfield set isactivestopwatch = 1, stopwatchtime= @stopwatchtime where position = @position and gametype = @gametype";
            command.Parameters.AddWithValue("@position", field.Position);
            command.Parameters.AddWithValue("@gametype", field.GameType);
            command.Parameters.AddWithValue("@stopwatchtime", DateTime.Now);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
