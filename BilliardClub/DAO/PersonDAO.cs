﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class PersonDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void AddPerson(Person person)
        {
            DatabaseConnection db = DatabaseConnection.GetConnection();
            MySqlCommand command = db.CreateCommand();
            command.CommandText = "Insert into Person(Name,Surname,DateOfBirth) values(@name,@surname,@date)";
            MySqlParameter name = new MySqlParameter() { ParameterName = "@name", Value = person.Name, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter surname = new MySqlParameter() { ParameterName = "@surname", Value = person.Surname, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter dateOfBirth = new MySqlParameter() { ParameterName = "@date", Value = person.DateOfBirth, MySqlDbType = MySqlDbType.Date };
            command.Parameters.AddRange(new object[] { name, surname, dateOfBirth });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
                person.ID = (int)command.LastInsertedId;
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }
        public void UpdatePersonInfo(Person person)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update person set Name = @Name, Surname = @Surname, DateOfBirth = @DateOfBirth where ID = @ID";
            command.Parameters.AddWithValue("@Name", person.Name);
            command.Parameters.AddWithValue("@Surname", person.Surname);
            command.Parameters.AddWithValue("@DateOfBirth", person.DateOfBirth);
            command.Parameters.AddWithValue("ID", person.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
