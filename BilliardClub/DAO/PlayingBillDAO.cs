﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class PlayingBillDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public void AddPlayingBill(PlayingBill bill)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into playingbill(Time,Value,ClubID) values(@time, @value, @clubid)";
            MySqlParameter time = new MySqlParameter() { ParameterName = "@time", Value = bill.Time, MySqlDbType = MySqlDbType.DateTime };
            MySqlParameter value = new MySqlParameter() { ParameterName = "@value", Value = bill.Value, MySqlDbType = MySqlDbType.Decimal };
            MySqlParameter clubid = new MySqlParameter() { ParameterName = "@clubid", Value = bill.ClubID, MySqlDbType = MySqlDbType.Int32 };
            command.Parameters.AddRange(new object[] { time, value, clubid });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
                bill.Id = (int)command.LastInsertedId;
            }catch(MySqlException ex)
            {
                log.Warn(ex);
            }
            bill.PlayingUnits.ForEach(b =>
            {
                b.PlayingBillID = bill.Id;
                new PlayingUnitDAO().AddPlayingUnit(b);
            });
        }

        public List<PlayingBill> GetBills(DateTime start, DateTime end)
        {
            List<PlayingBill> bills = new List<PlayingBill>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from playingbill where @Start <= time and @End >= time";
            command.Parameters.AddWithValue("@Start", start);
            command.Parameters.AddWithValue("@End", end);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                bills.Add(new PlayingBill
                {
                    Value = reader.GetDecimal("Value"),
                    ClubID = reader.GetInt32("ClubID"),
                    Id = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                    
                });
            }
            reader.Close();
            foreach(PlayingBill bill in bills)
            {
                bill.PlayingUnits = new PlayingUnitDAO().GetByID(bill.Id);
            }
            return bills;
        }
    }
}
