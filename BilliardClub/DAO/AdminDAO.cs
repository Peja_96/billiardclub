﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class AdminDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Admin GetAdmin(string str)
        {
            Admin admin = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from manager inner join employee on personid = employeepersonid inner join person on id = employeepersonid where username=" + "'" + str + "' and isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                admin = new Admin()
                {
                    Username = reader.GetString("Username"),
                    Password = reader.GetString("Password"),
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Surname = reader.GetString("Surname"),
                    Language = reader.GetString("Language"),
                    Theme = reader.GetString("Theme"),
                    DateOfBirth = reader.GetDateTime("DateOfBirth"),
                    EmploymentRecordBook = reader.GetString("EmploymentRecordBook"),
                    Salary = reader.GetDecimal("Salary")
                };
            }
            reader.Close();
            return admin;
        }

        public void AddAdmin(Admin admin)
        {
            new EmployeeDAO().AddEmployee(admin);
            MySqlCommand mySqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            mySqlCommand.CommandText = "insert into manager(EmployeePersonID,Username,Password) values(@employeePersonID, @username, @password)";
            MySqlParameter employeePersonID = new MySqlParameter() { ParameterName = "@employeePersonID", Value = admin.ID, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter username = new MySqlParameter() { ParameterName = "@username", Value = admin.Username, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter password = new MySqlParameter() { ParameterName = "@password", Value = admin.Password, MySqlDbType = MySqlDbType.VarChar };
            mySqlCommand.Parameters.AddRange(new object[] { employeePersonID, username, password });
            mySqlCommand.Prepare();
            try
            {
                mySqlCommand.ExecuteNonQuery();
            } catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public void ModifyAdmin(Admin admin)
        {
            new EmployeeDAO().UpdateEmployeeInfo(admin);
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update manager set username = @username, password = @password , theme = @theme , language= @language where employeepersonid=@id";
            command.Parameters.AddWithValue("@username", admin.Username);
            command.Parameters.AddWithValue("@password", admin.Password);
            command.Parameters.AddWithValue("@theme", admin.Theme);
            command.Parameters.AddWithValue("@language", admin.Language);
            command.Parameters.AddWithValue("@id", admin.ID);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public bool ExistAdmin(string str)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from manager inner join employee on personid = employeepersonid inner join person on id = employeepersonid where username=" + "'" + str + "' and isactive = 1";

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public string GetLangTheme(string str)
        {
            string language = "";
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from manager where username=" + "'" + str + "'";

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                language = reader.GetString("Language");
            }
            reader.Close();
            return language;
        }
    }
}
