﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class FinancialEntryDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public List<FinancialEntry> GetFinancialEntries()
        {
            List<FinancialEntry> financialEntries = new List<FinancialEntry>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from financialentry";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                financialEntries.Add(new FinancialEntry
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Description = reader.GetString("Type"),
                    Count = reader.GetInt32("Count"),
                    ID = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                });
            }
            reader.Close();

            return financialEntries;
        }

        public List<FinancialEntry> GetFinancialEntriesByDate(DateTime start, DateTime end)
        {
            List<FinancialEntry> financialEntries = new List<FinancialEntry>();

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from financialentry where @Start <= time and @End >= time";
            command.Parameters.AddWithValue("@Start", start);
            command.Parameters.AddWithValue("@End", end);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                financialEntries.Add(new FinancialEntry
                {
                    Amount = reader.GetDecimal("Amount"),
                    ClubID = reader.GetInt32("ClubID"),
                    Description = reader.GetString("Type"),
                    Count = reader.GetInt32("Count"),
                    ID = reader.GetInt32("ID"),
                    Time = reader.GetDateTime("Time"),
                });
            }
            reader.Close();

            return financialEntries;
        }
        public void AddFinancialEntry(FinancialEntry financialEntry)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into financialentry(clubid, time, count, amount,type) values (@ClubID, @Time, @Count, @Amount, @Type)";
            command.Parameters.AddWithValue("@ClubID", financialEntry.ClubID);
            command.Parameters.AddWithValue("@Type", financialEntry.Description);
            command.Parameters.AddWithValue("@Count", financialEntry.Count);
            command.Parameters.AddWithValue("@Time", financialEntry.Time);
            command.Parameters.AddWithValue("@Amount", financialEntry.Amount);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
                financialEntry.ID = (int)command.LastInsertedId;
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
