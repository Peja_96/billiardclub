﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class ReservationDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public void AddReservation(Reservation reservation)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "insert into reservation(name, time, playingfieldgametype, playingfieldposition) values(@name, @time, @type, @position)";
            MySqlParameter name = new MySqlParameter() { ParameterName = "@name", Value = reservation.Name, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter time = new MySqlParameter() { ParameterName = "@time", Value = reservation.DateTime, MySqlDbType = MySqlDbType.DateTime };
            MySqlParameter type = new MySqlParameter() { ParameterName = "@type", Value = reservation.GameType, MySqlDbType = MySqlDbType.Enum };
            MySqlParameter position = new MySqlParameter() { ParameterName = "@position", Value = reservation.Position, MySqlDbType = MySqlDbType.Int32 };
            command.Parameters.AddRange(new object[] { name, time, type, position });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }catch(MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public Reservation GetReservation(PlayingField playingField)
        {
            Reservation reservation = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from reservation where playingfieldposition = @position and playingfieldgametype = @type and isactive = 1";
            command.Parameters.AddWithValue("@position", playingField.Position);
            command.Parameters.AddWithValue("@type", playingField.GameType);

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                reservation = new Reservation()
                {
                    DateTime = reader.GetDateTime("Time"),
                    Name = reader.GetString("Name"),
                    Position = playingField.Position,
                    GameType = playingField.GameType
                };
            }
            reader.Close();
            return reservation;
        }

        public Reservation GetReservation(int position, string gameType)
        {
            Reservation reservation = null;
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from reservation where playingfieldposition = @position and playingfieldgametype = @type and isactive = 1";
            command.Parameters.AddWithValue("@position", position);
            command.Parameters.AddWithValue("@type", gameType);

            MySqlDataReader reader = command.ExecuteReader();
            if (reader.Read())
            {
                reservation = new Reservation()
                {
                    DateTime = reader.GetDateTime("Time"),
                    Name = reader.GetString("Name"),
                    Position = position,
                    GameType = gameType
                };
            }
            reader.Close();
            return reservation;
        }

        public void DeleteResrvation(Reservation reservation)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update reservation set isactive = 0 where PlayingFieldPosition = @position and PlayingFieldGameType = @gametype";
            command.Parameters.AddWithValue("@position", reservation.Position);
            command.Parameters.AddWithValue("@gametype", reservation.GameType);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
        public bool IsDeactivatedReservation(Reservation reservation)
        {

            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from reservation where PlayingFieldPosition = @position and PlayingFieldGameType = @gametype and Time = @time and isactive = 0";
            command.Parameters.AddWithValue("@position", reservation.Position);
            command.Parameters.AddWithValue("@time", reservation.DateTime);
            command.Parameters.AddWithValue("@gametype", reservation.GameType);
            command.Prepare();

            MySqlDataReader reader = command.ExecuteReader();

            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public void UpdateReservation(Reservation reservation)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update reservation set isactive = 1 where PlayingFieldPosition = @position and PlayingFieldGameType = @gametype";
            command.Parameters.AddWithValue("@position", reservation.Position);
            command.Parameters.AddWithValue("@gametype", reservation.GameType);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }

    }
}
