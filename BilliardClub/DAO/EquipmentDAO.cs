﻿using BilliardClub.DTO;
using log4net;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace BilliardClub.DAO
{
    class EquipmentDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public List<Equipment> GetEquipment()
        {
            List<Equipment> equipment = new List<Equipment>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from equipment where quantity > 0 ";

            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                equipment.Add(new Equipment()
                {
                    ID = reader.GetInt32("ID"),
                    EquipmentType = reader.GetString("Type"),
                    Quantity = reader.GetInt32("Quantity"),
                    ClubID = reader.GetInt32("ClubID")
                });
            }
            reader.Close();

            return equipment;
        }

        public List<Equipment> GetEquipmentByType(string type)
        {
            List<Equipment> equipment = new List<Equipment>();
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "select * from equipment where type like @TypeParam and quantity > 0";
            command.Parameters.AddWithValue("@TypeParam", new Regex("%" + type + "%"));
            MySqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                equipment.Add(new Equipment()
                {
                    ID = reader.GetInt32("ID"),
                    EquipmentType = reader.GetString("Type"),
                    Quantity = reader.GetInt32("Quantity"),
                    ClubID = reader.GetInt32("ClubID")
                });
            }
            reader.Close();
            return equipment;

        }
        public void UpdateEquipment(Equipment equipment)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update equipment set Quantity = @Quantity where id = @ID;";
            command.Parameters.AddWithValue("@Quantity", equipment.Quantity);
            command.Parameters.AddWithValue("@ID", equipment.ID);
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                log.Warn(ex);
            }
        }

        public void AddEquipment(Equipment equipment)
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "Insert into equipment(Type, Quantity, ClubID) values(@type, @quantity, @clubid)";
            MySqlParameter type = new MySqlParameter() { ParameterName = "@type", Value = equipment.EquipmentType, MySqlDbType = MySqlDbType.VarChar };
            MySqlParameter quantity = new MySqlParameter() { ParameterName = "@quantity", Value = equipment.Quantity, MySqlDbType = MySqlDbType.Int32 };
            MySqlParameter clubid = new MySqlParameter() { ParameterName = "@clubid", Value = equipment.ClubID, MySqlDbType = MySqlDbType.Int32 };
            command.Parameters.AddRange(new object[] { type, quantity, clubid });
            command.Prepare();
            try
            {
                command.ExecuteNonQuery();
                equipment.ID = (int)command.LastInsertedId;
            }catch(MySqlException ex)
            {
                log.Warn(ex);
            }
        }
    }
}
