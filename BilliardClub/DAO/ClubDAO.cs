﻿using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using log4net;

namespace BilliardClub.DAO
{
    class ClubDAO
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public Club ReadClubInfo()
        {
            Club club = null;
            MySqlCommand mySqlCommand = DatabaseConnection.GetConnection().CreateCommand();
            mySqlCommand.CommandText = "select * from club";
            MySqlDataReader reader = mySqlCommand.ExecuteReader();
            
            if (reader.Read())
            {
                club = new Club()
                {
                    ID = reader.GetInt32("ID"),
                    Name = reader.GetString("Name"),
                    Address = reader.GetString("Address"),
                    OpenTime = reader.GetDateTime("OpenTime"),
                    CloseTime = reader.GetDateTime("CloseTime")
                };
            }
            reader.Close();
            return club;
        }
        public void ModifyClubInfo()
        {
            MySqlCommand command = DatabaseConnection.GetConnection().CreateCommand();
            command.CommandText = "update club set name = @Name, address = @Address, opentime = @OpenTime, closetime = @CloseTime where id = @ID";
            command.Parameters.AddWithValue("@Name", Club.BilliardClub.Name);
            command.Parameters.AddWithValue("@Address", Club.BilliardClub.Address);
            command.Parameters.AddWithValue("@OpenTime", Club.BilliardClub.OpenTime);
            command.Parameters.AddWithValue("@CloseTime", Club.BilliardClub.CloseTime);
            command.Parameters.AddWithValue("@ID", Club.BilliardClub.ID);
            command.Prepare();

            try
            {
                command.ExecuteNonQuery();
            }
            catch (MySqlException mysqlex)
            {
                log.Warn(mysqlex);
            }
        }
    }
}
