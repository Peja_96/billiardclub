﻿#pragma checksum "..\..\..\GUI\AddEmoloyeeWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3EF7889BD0301B7AB7880CF8632575F87FF5839A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using BilliardClub.GUI;
using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace BilliardClub.GUI {
    
    
    /// <summary>
    /// AddEmoloyeeWindow
    /// </summary>
    public partial class AddEmoloyeeWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 14 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox nameBox;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox surnameBox;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox recordBookBox;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox salaryBox;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox dateOfBirthBox;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ButtonAddEmployee;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/BilliardClub;component/gui/addemoloyeewindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.nameBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 2:
            this.surnameBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 3:
            this.recordBookBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 19 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
            this.recordBookBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.recordBookBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 4:
            this.salaryBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 23 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
            this.salaryBox.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.salaryBox_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 5:
            this.dateOfBirthBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.ButtonAddEmployee = ((System.Windows.Controls.Button)(target));
            
            #line 29 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
            this.ButtonAddEmployee.Click += new System.Windows.RoutedEventHandler(this.ButtonAddEmployee_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 30 "..\..\..\GUI\AddEmoloyeeWindow.xaml"
            ((System.Windows.Controls.MenuItem)(target)).Click += new System.Windows.RoutedEventHandler(this.closeButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

