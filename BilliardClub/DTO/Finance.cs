﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Finance
    {
        private int year;
        private decimal amount;
        private int clubID;
        private decimal income;
        private decimal outcome;

        public int Year { get => year; set => year = value; }
        public decimal Amount { get => amount; set => amount = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public decimal Income { get => income; set => income = value; }
        public decimal Outcome { get => outcome; set => outcome = value; }
    }


}

