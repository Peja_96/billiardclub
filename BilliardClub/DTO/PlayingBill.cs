﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class PlayingBill
    {
        private int id;
        private DateTime time;
        private decimal value;
        private int clubID;
        private List<PlayingUnit> playingUnits;

        public DateTime Time { get => time; set => time = value; }
        public decimal Value { get => value; set => this.value = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public List<PlayingUnit> PlayingUnits { get => playingUnits; set => playingUnits = value; }
        public int Id { get => id; set => id = value; }
    }
}
