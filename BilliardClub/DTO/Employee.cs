﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Employee : Person
    {
        private string employmentRecordBook;
        private int clubID;
        private decimal salary;

        public Employee(int IdPerson, string Name, string Surname, DateTime DateOfBirth, string EmploymentRecordBook) : base(IdPerson, Name, Surname, DateOfBirth)
        {
            this.EmploymentRecordBook = EmploymentRecordBook;
        }

        public Employee() { }

        public string EmploymentRecordBook { get => employmentRecordBook; set => employmentRecordBook = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public decimal Salary { get => salary; set => salary = value; }
    }

}
