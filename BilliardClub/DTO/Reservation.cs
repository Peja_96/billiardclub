﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BilliardClub.DAO;

namespace BilliardClub.DTO
{
    public class Reservation
    {
        private string name;
        private DateTime dateTime;
        private int position;
        private string gameType;

        public string Name { get => name; set => name = value; }
        public DateTime DateTime { get => dateTime; set => dateTime = value; }
        
        public int Position { get => position; set => position = value; }
        public string GameType { get => gameType; set => gameType = value; }

        
    }
}
