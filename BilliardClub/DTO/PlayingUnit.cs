﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class PlayingUnit
    {
        private int playingBillID;
        private string playingFieldGameType;
        private int playingFieldPosition;
        private TimeSpan timePlayed;
        private decimal value;

        public int PlayingBillID { get => playingBillID; set => playingBillID = value; }
        public TimeSpan TimePlayed { get => timePlayed; set => timePlayed = value; }
        public decimal Value { get => value; set => this.value = value; }
        public string PlayingFieldGameType { get => playingFieldGameType; set => playingFieldGameType = value; }
        public int PlayingFieldPosition { get => playingFieldPosition; set => playingFieldPosition = value; }
    }
}
