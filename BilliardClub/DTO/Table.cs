﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Table : PlayingField
    {
        
        enum Types { Billiard, Snooker}
        private Types type;

        public Table(int i) { type = (Types)i; }
        public Table(int i,string str,int position):base(str,position)
        {
            type = (Types)i;
            
        }

        
        public string Type { get => type.ToString(); }
    }
}
