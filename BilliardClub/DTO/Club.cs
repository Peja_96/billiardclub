﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Club
    {
        private int id;
        private string name;
        private string address;
        private string[] telephones;
        private DateTime openTime;
        private DateTime closeTime;
        private List<Game> games;

        public string Name { get => name; set => name = value; }
        public string Address { get => address; set => address = value; }
        public string[] Telephones { get => telephones; set => telephones = value; }
        public DateTime OpenTime { get => openTime; set => openTime = value; }
        public DateTime CloseTime { get => closeTime; set => closeTime = value; }
        public int ID { get => id; set => id = value; }
        public List<Game> Games { get => games; set => games = value; }

        public static Club BilliardClub { get; set; }
    }
}
