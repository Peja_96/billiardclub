﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Game
    {
        private string type;
        private decimal pricePerOur;
        private int clubID;

        public string Type { get => type; set => type = value; }
        public decimal PricePerOur { get => pricePerOur; set => pricePerOur = value; }
        public int ClubID { get => clubID; set => clubID = value; }

        
    }
}
