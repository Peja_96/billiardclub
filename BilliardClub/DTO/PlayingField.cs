﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using BilliardClub.DAO;


namespace BilliardClub.DTO
{
    public class PlayingField 
    {
        private string gameType;
        private Reservation reservation;
        private Stopwatch stopwatch;
        private DateTime stopwatchTime;
        private bool isActive;
        private string image;
        private int position;
        private TimeSpan timePlayed;
        
        public PlayingField() {
            stopwatch = new Stopwatch();
        }
        public PlayingField(string str,int position)
        {
            stopwatch = new Stopwatch();
            image = str;
            this.position = position;
            timePlayed = new TimeSpan(0);
        }

        public Reservation Reservation { get => reservation; set => reservation = value; }
        public Stopwatch Stopwatch { get => stopwatch; set => stopwatch = value; }
        public DateTime StopwatchTime { get => stopwatchTime; set => stopwatchTime = value; }
        public string Image { get => image; set => image = value; }
        public int Position { get => position; set => position = value; }
        public string GameType { get => gameType; set => gameType = value; }

        public dynamic GameTypeLanguage
        {
            get => (string)App.Current.FindResource(GameType);
        }
        public bool IsActive { get => isActive; set => isActive = value; }
        public TimeSpan TimePlayed { get => timePlayed; set => timePlayed = value; }

        public decimal GetCurrentPrice()
        {
            decimal temp = 0;
            List<Game> games = new GameDAO().GetGames();
            games.ForEach(g =>
            {
                if (g.Type.Equals(GameType))
                    temp =  g.PricePerOur;
            });
            return temp;
        }

        
    }
}
