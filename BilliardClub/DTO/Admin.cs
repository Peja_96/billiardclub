﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Admin : Employee
    {
        private string username;
        private string password;
        private string language;
        private string theme;

        public Admin() { }

        public Admin(int IdPerson, string Name, string Surname, DateTime DateOfBirth, string EmploymentRecordBook, string username, string password) : base(IdPerson, Name, Surname, DateOfBirth, EmploymentRecordBook)
        {
            this.username = username;
            this.password = password;
        }
        public string Username { get => username; set => username = value; }
        public string Password { get => password; set => password = value; }
        public string Language { get => language; set => language = value; }
        public string Theme { get => theme; set => theme = value; }
    }
}
