﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class DartBoard : PlayingField
    {
        enum Types { Classic, BlackWhite}
        private Types type;

        public DartBoard() { }
        public DartBoard(int i) { type = (Types)i; }
        public DartBoard(int i,string str, int position ):base(str, position)
        {
            type = (Types)i;
        }

        public string Type { get => type.ToString(); }
    }
}
