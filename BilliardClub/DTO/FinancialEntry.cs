﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    class FinancialEntry
    {
        private int id;
        private int clubID;
        private string description;
        private int count;
        private DateTime time;
        private decimal amount;
        

        public int ID { get => id; set => id = value; }
        public int ClubID { get => clubID; set => clubID = value; }
        public string Description { get => description; set => description = value; }
        public int Count { get => count; set => count = value; }
        public DateTime Time { get => time; set => time = value; }
        public decimal Amount { get => amount; set => amount = value; }
        
    }
}
