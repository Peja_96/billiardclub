﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.DTO
{
    public class Equipment
    {
        private int id;
        private string equipmentType;
        private int quantity;
        private int clubID;

        public int ID { get => id; set => id = value; }
        public string EquipmentType { get => equipmentType; set => equipmentType = value; }
        public int Quantity { get => quantity; set => quantity = value; }
        public int ClubID { get => clubID; set => clubID = value; }

        public Equipment(int equipmentID, string equipmentType, int quantity, int clubID)
        {
            this.ID = equipmentID;
            this.EquipmentType = equipmentType;
            this.Quantity = quantity;
            this.ClubID = clubID;
        }

        public Equipment() { }
    }

}

