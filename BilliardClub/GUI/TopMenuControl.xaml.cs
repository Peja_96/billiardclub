﻿using BilliardClub.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for TopMenuControl.xaml
    /// </summary>
    public partial class TopMenuControl : UserControl
    {
        public TopMenuControl()
        {
            InitializeComponent();
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Windows[0].Close();
        }

        private void ChangeTheme(object sender, RoutedEventArgs e)
        {
           


            switch (int.Parse(((MenuItem)sender).Uid))
            {
                // light
                case 0:
                    {
                        if (Application.Current.Windows[0] is AdminWindow)
                        {
                            AdminWindow.admin.Theme = "Light";
                            new AdminDAO().ModifyAdmin(AdminWindow.admin);
                        }
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });
                    } break;
                // colourful light
                case 1:
                    {
                        if (Application.Current.Windows[0] is AdminWindow)
                        {
                            AdminWindow.admin.Theme = "Dark";
                            new AdminDAO().ModifyAdmin(AdminWindow.admin);
                        }
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Remove( new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove( new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });
                    } break;
                case 2:
                    {
                        if (Application.Current.Windows[0] is AdminWindow)
                        {
                            AdminWindow.admin.Theme = "Black";
                            new AdminDAO().ModifyAdmin(AdminWindow.admin);
                        }
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });
                    }
                    break;
                    // dark

            }
            e.Handled = true;

        }

        private void ChangeLanguage(object sender, RoutedEventArgs e)
        {
            switch (int.Parse(((MenuItem)sender).Uid))
            {
                // light
                case 0:
                    {
                        if (Application.Current.Windows[0] is AdminWindow)
                        {
                            AdminWindow.admin.Language = "English";
                            new AdminDAO().ModifyAdmin(AdminWindow.admin);
                        }
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/EnglishDictionary.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/SerbianDictionary.xaml", UriKind.RelativeOrAbsolute) });
                    }
                    break;
                // colourful light
                case 1:
                    {
                        if (Application.Current.Windows[0] is AdminWindow)
                        {
                            AdminWindow.admin.Language = "English";
                            new AdminDAO().ModifyAdmin(AdminWindow.admin);
                        }
                        Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/SerbianDictionary.xaml", UriKind.RelativeOrAbsolute) });
                        Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/EnglishDictionary.xaml", UriKind.RelativeOrAbsolute) });
                    }
                    break;
                    // dark

            }
            e.Handled = true;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (Application.Current.Windows[0].WindowState == WindowState.Normal)
            {
                Application.Current.Windows[0].WindowState = WindowState.Maximized;
                iconKind.Kind = MaterialDesignThemes.Wpf.PackIconKind.WindowRestore;
            }
            else
            {
                Application.Current.Windows[0].WindowState = WindowState.Normal;
                iconKind.Kind = MaterialDesignThemes.Wpf.PackIconKind.WindowMaximize;
            }
        }

        private void minimize_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Windows[0].WindowState = WindowState.Minimized;
        }
    }
}
