﻿using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for PlayingBillWindow.xaml
    /// </summary>
    public partial class PlayingBillWindow : Window
    {
        PlayingBill bill;
        public PlayingBillWindow(PlayingBill bill)
        {
            InitializeComponent();
            this.bill = bill;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            bill.PlayingUnits.ForEach(b =>
            {
                unitsPanel.Children.Add(new PlayingUnitControl(b));
            });
            priceBox.Text = String.Format("{0:0.00}", bill.Value);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void ChargeButton_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
            Close();
        }
    }
}
