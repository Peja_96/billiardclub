﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for AdminInfoControl.xaml
    /// </summary>
    public partial class AdminInfoControl : UserControl
    {
        Admin admin;
        public AdminInfoControl()
        {
            InitializeComponent();
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            new AddAdminWindow().ShowDialog();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            admin = AdminWindow.admin;
            nameBox.Text = admin.Name;
            surnameBox.Text = admin.Surname;
            usernameBox.Text = admin.Username;
            passwordBox.Password = admin.Password;
            recordBookBox.Text = admin.EmploymentRecordBook;
            salaryBox.Text = admin.Salary.ToString();
            dateBox.Text = admin.DateOfBirth.ToString("dd.MM.yyyy.");
        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(nameBox.Text) || string.IsNullOrEmpty(surnameBox.Text) || string.IsNullOrEmpty(usernameBox.Text) ||
                        string.IsNullOrEmpty(recordBookBox.Text) || !DateTime.TryParse(dateBox.Text, out DateTime dateEmp) || !Decimal.TryParse(salaryBox.Text, out decimal salary)
                        || !recordBookBox.Text.All(char.IsDigit))
            {
                new CustomMessageBox("Wrong Data", "Please enter required information correctly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            else
            {
                CheckPassword();

                admin.Name = nameBox.Text;
                admin.Surname = surnameBox.Text;
                admin.Username = usernameBox.Text;
                admin.EmploymentRecordBook = recordBookBox.Text;
                admin.Salary = Decimal.Parse(salaryBox.Text);
                admin.DateOfBirth = DateTime.Parse(dateBox.Text);

                new AdminDAO().ModifyAdmin(admin);
            }
        }

        private void visibilityIcon_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (passwordBox.Visibility == Visibility.Visible)
            {
                passwordBox.Visibility = Visibility.Hidden;
                textBox.Text = passwordBox.Password;
                textBox.Visibility = Visibility.Visible;
                visibilityIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Visibility;
            }
            else
            {
                textBox.Visibility = Visibility.Hidden;
                passwordBox.Password = textBox.Text;
                passwordBox.Visibility = Visibility.Visible;
                visibilityIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VisibilityOff;
            }
        }

        private void visibilityIcon1_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (repeatPasswordBox.Visibility == Visibility.Visible)
            {
                repeatPasswordBox.Visibility = Visibility.Hidden;
                repeatTextBox.Text = repeatPasswordBox.Password;
                repeatTextBox.Visibility = Visibility.Visible;
                visibilityIcon1.Kind = MaterialDesignThemes.Wpf.PackIconKind.Visibility;
            }
            else
            {
                repeatTextBox.Visibility = Visibility.Hidden;
                repeatPasswordBox.Password = repeatTextBox.Text;
                repeatPasswordBox.Visibility = Visibility.Visible;
                visibilityIcon1.Kind = MaterialDesignThemes.Wpf.PackIconKind.VisibilityOff;
            }
        }

        private void CheckPassword()
        {
            string oldPassword;
            string newPassword;
            if (passwordBox.Visibility == Visibility.Visible)
                oldPassword = passwordBox.Password;
            else
                oldPassword = textBox.Text;
            if (repeatPasswordBox.Visibility == Visibility.Visible)
                newPassword = repeatPasswordBox.Password;
            else
                newPassword = repeatTextBox.Text;

            if (oldPassword.Equals(newPassword))
                admin.Password = newPassword;
        }
    }
}
