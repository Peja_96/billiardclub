﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for AddEquipmentWindow.xaml
    /// </summary>
    public partial class AddEquipmentWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public AddEquipmentWindow()
        {
            InitializeComponent();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;

        }

        private void priceBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValidDecimal(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValidDecimal(string str)
        {
            decimal i;
            return decimal.TryParse(str, out i) && i > 0;

        }

        private void addEquipmentButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(typeBox.Text))
            {
                try
                {
                    Equipment equipment = new Equipment()
                    {
                        EquipmentType = typeBox.Text,
                        Quantity = Int32.Parse(quantityBox.Text),
                        ClubID = Club.BilliardClub.ID
                    };
                    EquipmentDAO equipmentDAO = new EquipmentDAO();
                    List<Equipment> list = equipmentDAO.GetEquipmentByType(typeBox.Text);
                    if (list.Count == 0)
                        equipmentDAO.AddEquipment(equipment);
                    else
                    {
                        int pom = 0;
                        list.ForEach(p =>
                        {
                            if (p.EquipmentType.ToLower().Equals(equipment.EquipmentType.ToLower()) && pom == 0)
                            {
                                equipment.ID = p.ID;
                                equipment.Quantity += p.Quantity;
                                equipmentDAO.UpdateEquipment(equipment);
                                pom = 1;
                            }

                        });
                        if (pom == 0)
                            equipmentDAO.AddEquipment(equipment);
                    }
                    new FinancialEntryDAO().AddFinancialEntry(new FinancialEntry()
                    {
                        ClubID = Club.BilliardClub.ID,
                        Time = DateTime.Now,
                        Description = "Kupovina igrackog terena",
                        Count = Int32.Parse(quantityBox.Text),
                        Amount = Decimal.Parse(priceBox.Text)
                    });
                    DialogResult = true;
                    Close();
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                    new CustomMessageBox("Error", "Error during adding equipment", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Error", "Error during adding equipment", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
    }
}
