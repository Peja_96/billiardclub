﻿using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for PlayingUnitControl.xaml
    /// </summary>
    public partial class PlayingUnitControl : UserControl
    {
        PlayingUnit unit;
        public PlayingUnitControl(PlayingUnit unit)
        {
            InitializeComponent();
            this.unit = unit;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            typeBox.SetResourceReference(TextBox.TextProperty,unit.PlayingFieldGameType);
            timeBox.Text = String.Format("{0:00}:{1:00}", unit.TimePlayed.Hours, unit.TimePlayed.Minutes);
            priceBox.Text = String.Format("{0:0.00}", unit.Value);
        }
    }
}
