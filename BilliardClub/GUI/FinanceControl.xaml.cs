﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for FinanceControl.xaml
    /// </summary>
    public partial class FinanceControl : UserControl
    {
        public FinanceControl()
        {
            InitializeComponent();
            
        }


        public class MyViewModel
        {
            public ObservableCollection<MyData> Data { get; set; }

            public MyViewModel()
            {

            }
            public void AddOutcome(Finanaces finanaces)
            {
                Data = new ObservableCollection<MyData>
                {
                    new MyData {Name=Application.Current.FindResource("EmployeeSalaries") as string, Count = finanaces.GetSalaries()},
                    new MyData {Name=Application.Current.FindResource("EquipmentCost") as string, Count = finanaces.GetFinancialEntries()},
                };
            }
            public void AddIncome(decimal billiard, decimal snooker, decimal darts, decimal blackDarts)
            {
                Data = new ObservableCollection<MyData>
                {
                    new MyData {Name= Application.Current.FindResource("Billiard") as string, Count = billiard},
                    new MyData {Name= Application.Current.FindResource("Snooker") as string, Count = snooker},
                    new MyData {Name= Application.Current.FindResource("Darts") as string , Count = darts},
                    new MyData {Name= Application.Current.FindResource("BlackDarts") as string, Count = blackDarts},
                };
            }
        }
        public class MyData
        {

            public string Name { get; internal set; }
            public decimal Count { get; internal set; }
        }

        private void BntCalculate_Click(object sender, RoutedEventArgs e)
        {
            if (DateTime.TryParse(startDateBox.Text, out DateTime startDate) &&
                    startDate.CompareTo(new DateTime(1901, 1, 1)) > 0 && DateTime.TryParse(endDateBox.Text, out DateTime endDate)
                    && endDate.CompareTo(new DateTime(1901, 1, 1)) > 0)
            {
                if (endDate.CompareTo(startDate) > 0)
                {
                    Finanaces finanaces = new Finanaces();
                    MyViewModel income = new MyViewModel();
                    income.AddIncome(finanaces.GetIncomesByTypeDate("Billiard",startDate,endDate), finanaces.GetIncomesByTypeDate("Snooker", startDate, endDate),
                        finanaces.GetIncomesByTypeDate("Darts", startDate, endDate), finanaces.GetIncomesByTypeDate("BlackDarts", startDate, endDate));
                    mcIncome.DataContext = income;
                    MyViewModel outcome = new MyViewModel();
                    outcome.AddOutcome(finanaces);
                    mcOutcome.DataContext = outcome;
                    
                }
                else
                {
                    new CustomMessageBox("Error Date", "End date is before start date", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
            }
            else
            {
                new CustomMessageBox("Error Date", "Date is not valid", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void tbxStartDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void tbxEndDate_PreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private void tbxEndDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void tbxStartDate_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Finanaces finanaces = new Finanaces();
            MyViewModel income = new MyViewModel();
            income.AddIncome(finanaces.GetIncomesByType("Billiard"), finanaces.GetIncomesByType("Snooker"),
                        finanaces.GetIncomesByType("Darts"), finanaces.GetIncomesByType("BlackDarts"));
            mcIncome.DataContext = income;
            MyViewModel outcome = new MyViewModel();
            outcome.AddOutcome(finanaces);
            mcOutcome.DataContext = outcome;
        }
    }
}
