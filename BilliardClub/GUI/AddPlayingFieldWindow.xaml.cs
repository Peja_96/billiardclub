﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for AddPlayingFieldWindow.xaml
    /// </summary>
    public partial class AddPlayingFieldWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public AddPlayingFieldWindow()
        {
            InitializeComponent();
            
        }
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void addFieldButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string type = "";
                switch (gameTypeCmbx.SelectedIndex)
                {
                    case 0:
                        type = "Billiard";
                        break;
                    case 1:
                        type = "Snooker";
                        break;
                    case 2:
                        type = "Darts";
                        break;
                    case 3:
                        type = "BlackDarts";
                        break;
                }
                    
                PlayingField field = new PlayingField()
                {
                    Image = "C:/Users/Nikola/Desktop/Baze/BilliardClub/BilliardClub/GUI/Billiard.png",
                    Position = Int32.Parse(positionBox.Text),
                    GameType = type
                };
                List<PlayingField> fields = new PlayingFieldDAO().GetPlayingFields();
                fields.ForEach(f =>
                {
                    if (f.Position == field.Position && f.GameType == field.GameType)
                        throw new ExistException("Field already exist");
                });
                PlayingFieldDAO fieldDAO = new PlayingFieldDAO();
                if (fieldDAO.IsDeactivatedField(field))
                    fieldDAO.UpdateField(field);
                else
                    fieldDAO.AddField(field);
                new FinancialEntryDAO().AddFinancialEntry(new FinancialEntry()
                {
                    ClubID = Club.BilliardClub.ID,
                    Time = DateTime.Now,
                    Description = "Kupovina igrackog terena",
                    Count = 1,
                    Amount = Decimal.Parse(priceBox.Text)
                });
                DialogResult = true;
            }
            catch(ExistException myEx)
            {
                log.Warn(e);
                new CustomMessageBox("Error", myEx.Message, CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();

            }
            catch (Exception ex)
            {
                log.Warn(ex);
                new CustomMessageBox("Error", "Error during adding tables", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }

        }

        private void positionBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }
        public bool IsValid(string str)
        {
            int i;
            return int.TryParse(str, out i) && i > 0;

        }

        private void priceBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValidDecimal(((TextBox)sender).Text + e.Text);
        }

        public bool IsValidDecimal(string str)
        {
            decimal i;
            return decimal.TryParse(str, out i) && i > 0;

        }
    }
    
}
