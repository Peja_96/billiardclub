﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for ClubInfoControl.xaml
    /// </summary>
    public partial class ClubInfoControl : UserControl
    {
        
        public ClubInfoControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Club.BilliardClub = new ClubDAO().ReadClubInfo();
            InsertData();
        }
        private void InsertData()
        {
            clubNameBox.Text = Club.BilliardClub.Name;
            adressBox.Text = Club.BilliardClub.Address;
            openTimeBox.Text = Club.BilliardClub.OpenTime.ToString("hh:mm");
            closeTimeBox.Text = Club.BilliardClub.CloseTime.ToString("hh:mm");
            new TelephoneDAO().GetTelephones().ForEach(t =>
            {
                telephoneCmbx.Items.Add(t);
            });
            
        }

        private void changeButton_ContextMenuClosing(object sender, ContextMenuEventArgs e)
        {

        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckData())
            {
                Club.BilliardClub.Name = clubNameBox.Text;
                Club.BilliardClub.Address = adressBox.Text;
                Club.BilliardClub.OpenTime = DateTime.Parse(openTimeBox.Text);
                Club.BilliardClub.CloseTime = DateTime.Parse(closeTimeBox.Text);
                new ClubDAO().ModifyClubInfo();
            }
            else
            {
                new CustomMessageBox("Error", "Wrong data", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void PackIcon_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (CorrectPhone())
            {
                
                new TelephoneDAO().AddTelephone(telephoneBox.Text);
                telephoneCmbx.Items.Clear();
                new TelephoneDAO().GetTelephones().ForEach(t =>
                {
                    telephoneCmbx.Items.Add(t);
                });
                telephoneBox.Clear();
            }
           
            else
                new CustomMessageBox("Error", "Wrong phone number", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
        }

        private void PackIcon_PreviewMouseDown_1(object sender, MouseButtonEventArgs e)
        {
            new TelephoneDAO().RemoveTelephone(telephoneCmbx.SelectionBoxItem as string);
            telephoneCmbx.Items.RemoveAt(telephoneCmbx.SelectedIndex);
        }

        private bool CorrectPhone()
        {
            if (telephoneBox.Text.Length == 9 || telephoneBox.Text.Length == 13)
                return true;
            return false;

        }
        private bool CheckData()
        {
            
            if (!string.IsNullOrEmpty(clubNameBox.Text) && !string.IsNullOrEmpty(adressBox.Text)&&
                DateTime.TryParse(openTimeBox.Text, out DateTime date)
                && DateTime.TryParse(closeTimeBox.Text, out DateTime date1)
)
                return true;
            return false;

        }

        private void telephoneBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }
        public static bool IsValid(string str)
        {
            Int64 i;
            return Int64.TryParse(str, out i) && i >= 0;

        }
    }
}
