﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using BilliardClub.DTO;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for ElementControl.xaml
    /// </summary>
    public partial class ElementControl : UserControl
    {

        PlayingField playingField;
        
        public ElementControl(PlayingField playingField)
        {
            InitializeComponent();
            this.playingField = playingField;
        }
        private void SetData()
        {
            image.Source = new BitmapImage(new Uri(playingField.Image,UriKind.RelativeOrAbsolute));
            number.Text = playingField.Position.ToString();
            if(playingField.GameType.Equals("Billiard") || playingField.GameType.Equals("Snooker"))
                table.SetResourceReference(TextBlock.TextProperty, "Table");
            else
                table.SetResourceReference(TextBlock.TextProperty, "Board");
            //playingField.Stopwatch.Start();
            ResetData();
            description.SetResourceReference(TextBlock.TextProperty, playingField.GameType);
        }

        public void ResetData()
        {
            if (playingField.Reservation != null)
                timePlayed.SetResourceReference(TextBlock.TextProperty, "ReservedTable");
            else if (playingField.IsActive)
                timePlayed.SetResourceReference(TextBlock.TextProperty, "ActiveTable");
            else
                timePlayed.SetResourceReference(TextBlock.TextProperty, "FreeTable");
        }

        private void mainBorder_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DependencyObject ucParent = Parent;

            while (!(ucParent is UserControl))
            {
                ucParent = LogicalTreeHelper.GetParent(ucParent);
            }
            GamesControl control = ucParent as GamesControl;
            control.rightGrid.Children.Clear();
            control.rightGrid.Children.Add(new ElementDetailsControl(playingField , this));
            
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetData();
        }
    }
}
