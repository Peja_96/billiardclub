﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for ElementDetailsControl.xaml
    /// </summary>
    public partial class ElementDetailsControl : UserControl
    {
        PlayingField playingField;
        ElementControl caller;
        DispatcherTimer dt = new DispatcherTimer();
        
        string currentTime = string.Empty;
        public ElementDetailsControl(PlayingField playingField , ElementControl caller)
        {
            InitializeComponent();
            this.playingField = playingField;
            this.caller = caller;
            
            SetData();
            dt.Tick += new EventHandler(dt_Tick);
            dt.Interval = new TimeSpan(0, 0, 0, 0, 1);
            
            
        }

        void dt_Tick(object sender, EventArgs e)
        {
            if (playingField.Stopwatch.IsRunning)
            {
                TimeSpan ts = playingField.Stopwatch.Elapsed + playingField.TimePlayed;
                currentTime = String.Format("{0:00}:{1:00}:{2:00}",
                ts.Hours, ts.Minutes, ts.Seconds );
                timePlayed.Text = currentTime;
                totalPrice.Text = String.Format("{0:0.00}", ts.Minutes * playingField.GetCurrentPrice() / 60 + ts.Hours * playingField.GetCurrentPrice());
            }

        }
        private void SetData()
        {
            if (playingField.IsActive && !playingField.Stopwatch.IsRunning)
            {
                playingField.TimePlayed = (DateTime.Now - playingField.StopwatchTime);
                playingField.Stopwatch.Start();
            }
                
            number.Text = playingField.Position.ToString();
            if (playingField.GameType.Equals("Billiard") || playingField.GameType.Equals("Snooker"))
                title.SetResourceReference(TextBlock.TextProperty, "Table");
            else
                title.SetResourceReference(TextBlock.TextProperty, "Board");
            ResetData();

        }

        private void ResetData()
        {
            if (playingField.IsActive)
            {
                dt.Start();
                reservationButton.Visibility = Visibility.Collapsed;
                cancelReservationButton.Visibility = Visibility.Collapsed;
                startButton.Visibility = Visibility.Collapsed;
                chargeButton.Visibility = Visibility.Visible;
                addToBillButton.Visibility = Visibility.Visible;
                timeBlock.SetResourceReference(TextBlock.TextProperty, "PlayingTime");
                priceBlock.SetResourceReference(TextBlock.TextProperty, "Price");
                timePlayed.Text = playingField.Stopwatch.Elapsed.ToString(@"hh\:mm\:ss");
                totalPrice.Text = (playingField.Stopwatch.Elapsed.TotalMinutes * 0.1).ToString();


            }
            else if (playingField.Reservation != null)
            {
                reservationButton.Visibility = Visibility.Collapsed;
                cancelReservationButton.Visibility = Visibility.Visible;
                startButton.Visibility = Visibility.Visible;
                chargeButton.Visibility = Visibility.Collapsed;
                addToBillButton.Visibility = Visibility.Collapsed;
                timeBlock.SetResourceReference(TextBlock.TextProperty, "ReservationTime");
                priceBlock.SetResourceReference(TextBlock.TextProperty, "ReservationName");
                timePlayed.Text = playingField.Reservation.DateTime.ToString("hh:mm");
                totalPrice.Text = playingField.Reservation.Name;
            }
            else
            {
                reservationButton.Visibility = Visibility.Visible;
                cancelReservationButton.Visibility = Visibility.Collapsed;
                startButton.Visibility = Visibility.Visible;
                chargeButton.Visibility = Visibility.Collapsed;
                addToBillButton.Visibility = Visibility.Collapsed;
                timeBlock.Text = null;
                priceBlock.Text = null;
                timePlayed.Text = null;
                totalPrice.Text = null;
            }
            
        }
       

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (playingField.Reservation != null)
                DeleteReservation();
            playingField.Stopwatch.Start();
            playingField.TimePlayed = new TimeSpan(0);
            playingField.IsActive = true;
            new PlayingFieldDAO().UpdateStopwatch(playingField);
            ResetData();
            RefreshParent();
        }

        private void cancelReservationButton_Click(object sender, RoutedEventArgs e)
        {
            new ReservationDAO().DeleteResrvation(playingField.Reservation);
            playingField.Reservation = null;
            ResetData();
            RefreshParent();
        }

        private void chargeButton_Click(object sender, RoutedEventArgs e)
        {
            PlayingBill bill = new PlayingBill()
            {
                Time = DateTime.Now,
                PlayingUnits = MainWindow.playingUnits,
                ClubID = Club.BilliardClub.ID
            };
            CreateBillUnit();

            decimal temp = 0;
            bill.PlayingUnits.ForEach(b => temp += b.Value);
            bill.Value = temp;
            if (new PlayingBillWindow(bill).ShowDialog().Value)
            {
                if (playingField.Reservation != null)
                    DeleteReservation();
                playingField.Stopwatch.Stop();
                ResetStopwatch();
                new PlayingBillDAO().AddPlayingBill(bill);
                MainWindow.playingUnits.Clear();
                ResetData();
                RefreshParent();
            }
            else
            {
                MainWindow.playingUnits.RemoveAt(MainWindow.playingUnits.Count - 1);

            }
        }

        private void reservationButton_Click(object sender, RoutedEventArgs e)
        {
            var result =  new ReservationWindow(playingField).ShowDialog().Value;
            if (result)
            {
                ResetData();
                RefreshParent();
            }
                
        }

        private void addToBillButton_Click(object sender, RoutedEventArgs e)
        {
            if (playingField.Reservation != null)
                DeleteReservation();
            playingField.Stopwatch.Stop();
            CreateBillUnit();
            ResetStopwatch();
            ResetData();
            RefreshParent();
        }

        private void CreateBillUnit() {
            
            MainWindow.playingUnits.Add(new PlayingUnit()
            {
                PlayingFieldGameType = playingField.GameType,
                PlayingFieldPosition = playingField.Position,
                TimePlayed = playingField.Stopwatch.Elapsed + playingField.TimePlayed,
                Value = (playingField.Stopwatch.Elapsed + playingField.TimePlayed).Minutes * playingField.GetCurrentPrice() / 60 
                + (playingField.Stopwatch.Elapsed + playingField.TimePlayed).Hours * playingField.GetCurrentPrice()
            });
        }

        private void ResetStopwatch()
        {
            playingField.Stopwatch.Reset();
            new PlayingFieldDAO().DeactivateStopwatch(playingField);
            playingField.IsActive = false;
        }

        private void DeleteReservation()
        {
            
            new ReservationDAO().DeleteResrvation(playingField.Reservation);
            playingField.Reservation = null;
        }

        private void RefreshParent()
        {
            caller.ResetData();
        }
    }
}
