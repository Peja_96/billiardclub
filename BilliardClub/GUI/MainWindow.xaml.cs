﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using BilliardClub.GUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static List<PlayingUnit> playingUnits = new List<PlayingUnit>();
        public MainWindow()
        {
            InitializeComponent();
            SetElements();

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void SetElements()
        {
            Club.BilliardClub = new ClubDAO().ReadClubInfo();
            topGrid.Children.Add(new TopMenuControl());
            GamesControl control = new GamesControl(new DTO.PlayingField() { GameType = "Billiard" });
            workingGrid.Children.Clear();
            workingGrid.Children.Add(control);
        }

        private void billiardButton_Click(object sender, RoutedEventArgs e)
        {
            GamesControl control = new GamesControl(new DTO.PlayingField() { GameType = "Billiard"});
            workingGrid.Children.Clear();
            workingGrid.Children.Add(control);
        }

        private void snookerButton_Click(object sender, RoutedEventArgs e)
        {
            GamesControl control = new GamesControl(new DTO.PlayingField() { GameType = "Snooker" });
            workingGrid.Children.Clear();
            workingGrid.Children.Add(control);
        }

        private void dartsButton_Click(object sender, RoutedEventArgs e)
        {
            GamesControl control = new GamesControl(new DTO.PlayingField() { GameType = "Darts" });
            workingGrid.Children.Clear();
            workingGrid.Children.Add(control);
        }
        private void darts1Button_Click(object sender, RoutedEventArgs e)
        {
            GamesControl control = new GamesControl(new DTO.PlayingField() { GameType = "BlackDarts" });
            workingGrid.Children.Clear();
            workingGrid.Children.Add(control);
        }
    }
}
