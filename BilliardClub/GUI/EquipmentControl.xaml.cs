﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for EquipmentControl.xaml
    /// </summary>
    public partial class EquipmentControl : UserControl
    {
        public EquipmentControl()
        {
            InitializeComponent();
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if (EquipmentTable.SelectedItem != null)
            {
                bool modifyResult = new RemoveEquipmentWindow((Equipment)EquipmentTable.SelectedItem).ShowDialog().Value;
                if (modifyResult)
                {
                    ImportData();
                }
            }
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            EquipmentTable.Items.Clear();
            List<Equipment> list = new EquipmentDAO().GetEquipmentByType(tbxSearch.Text);
            list.ForEach(p => EquipmentTable.Items.Add(p));

        }
        private void EmployeesTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            /* ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
             if (e.VerticalChange > 0)
             {
                 if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                 {
                     offset++;

                     List<Employee> list = new EmployeeDAO().GetTenEmployees(offset * 10);
                     if (list.Count == 0)
                     {
                         offset--;
                         return;
                     }
                     list.ForEach(p => EmployeesTable.Items.Add(p));
                 }
             }*/
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            var result = new AddEquipmentWindow().ShowDialog().Value;
            if (result)
                ImportData();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            
            ImportData();
        }

        private void ImportData()
        {
            EquipmentTable.Items.Clear();
            List<Equipment> equipment = new EquipmentDAO().GetEquipment();
            equipment.ForEach(e => EquipmentTable.Items.Add(e));
        }
    }
}
