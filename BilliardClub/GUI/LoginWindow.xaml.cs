﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Security.Cryptography;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public LoginWindow()
        {
            InitializeComponent();
        }

        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (CheckInputs())
            {
                new AdminWindow(userBox.Text).Show();
                Close();
            }
            else
            {
                new CustomMessageBox("Error", "Wrong info", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void continueButton_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            Close();
        }

        private bool CheckInputs()
        {
            
            string password = "";
            if (passwordBox.Visibility == Visibility.Visible)
                password = passwordBox.Password;
            else
                password = textBox.Text;
            Admin admin = new AdminDAO().GetAdmin(userBox.Text);
            if (admin == null || !admin.Password.Equals(password))
                return false;
            return true;
        }

        private void visibilityIcon_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if(passwordBox.Visibility == Visibility.Visible)
            {
                passwordBox.Visibility = Visibility.Hidden;
                textBox.Text = passwordBox.Password;
                textBox.Visibility = Visibility.Visible;
                visibilityIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.Visibility;
            }
            else
            {
                textBox.Visibility = Visibility.Hidden;
                passwordBox.Password = textBox.Text;
                passwordBox.Visibility = Visibility.Visible;
                visibilityIcon.Kind = MaterialDesignThemes.Wpf.PackIconKind.VisibilityOff;
            }
        }
    }
}
