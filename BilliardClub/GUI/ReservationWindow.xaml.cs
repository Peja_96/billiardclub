﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using BilliardClub.DTO;
using log4net;
using BilliardClub.DAO;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for ReservationWindow.xaml
    /// </summary>
    public partial class ReservationWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        PlayingField playingField;
        public ReservationWindow(PlayingField playingField)
        {
            InitializeComponent();
            this.playingField = playingField;
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void nameBox_GotFocus(object sender, RoutedEventArgs e)
        {
            //Process process = Process.Start(new ProcessStartInfo(((Environment.GetFolderPath(Environment.SpecialFolder.System) + @"\osk.exe"))));
        }

        private void addReservationButton_Click(object sender, RoutedEventArgs e)
        {
            if (!nameBox.Text.Equals("") && DateTime.TryParse(timePicker.SelectedTime.ToString(),out DateTime dateTime))
                try
                {
                    Reservation reservation = new Reservation()
                    {
                        Name = nameBox.Text,
                        DateTime = (DateTime)timePicker.SelectedTime,
                        Position = playingField.Position,
                        GameType = playingField.GameType
                    };
                    playingField.Reservation = reservation;
                    ReservationDAO reservationDAO = new ReservationDAO();
                    if (reservationDAO.IsDeactivatedReservation(reservation))
                        reservationDAO.UpdateReservation(reservation);
                    else
                        reservationDAO.AddReservation(reservation);
                    DialogResult = true;
                    Close();
                }
                catch (Exception ex)
                {
                    log.Warn(ex);
                    new CustomMessageBox("Error", "Wrong reservation info", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();

                }
            else {
                new CustomMessageBox("Error", "Wrong reservation info", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }
    }
}
