﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for AddEmoloyeeWindow.xaml
    /// </summary>
    public partial class AddEmoloyeeWindow : Window
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public AddEmoloyeeWindow()
        {
            InitializeComponent();
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }

        private void ButtonAddEmployee_Click(object sender, RoutedEventArgs e)
        {
                
                if (string.IsNullOrEmpty(nameBox.Text) || string.IsNullOrEmpty(surnameBox.Text) ||
                        string.IsNullOrEmpty(recordBookBox.Text) || !DateTime.TryParse(dateOfBirthBox.Text, out DateTime dateEmp) || !Decimal.TryParse(salaryBox.Text, out decimal salary)
                        || !recordBookBox.Text.All(char.IsDigit))
                {
                    new CustomMessageBox("Wrong Data", "Please enter required information correctly.", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                }
                else
                {
                    try
                    {
                    Employee newEmployee = new Employee
                    {
                        DateOfBirth = dateEmp,
                        EmploymentRecordBook = recordBookBox.Text,
                        Name = nameBox.Text,
                        Surname = surnameBox.Text,
                        Salary = salary
                        };
                    new EmployeeDAO().AddEmployee(newEmployee);
                    DialogResult = true;
                    Close();
                    }
                    catch (Exception ex)
                    {
                        log.Warn(ex);
                        new CustomMessageBox("Error", "Error during adding employee", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
                    }
                }
            
        }

        private void recordBookBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValid(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValid(string str)
        {
            return Int64.TryParse(str, out long i) && i >= 0;
        }

        private void salaryBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsValidDecimal(((TextBox)sender).Text + e.Text);
        }

        public static bool IsValidDecimal(string str)
        {
            return decimal.TryParse(str, out decimal i) && i > 0;

        }
    }
}
