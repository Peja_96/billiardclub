﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for GamesControl.xaml
    /// </summary>
    public partial class GamesControl : UserControl
    {
        public GamesControl(PlayingField playingField)
        {
            InitializeComponent();
            SetData(playingField);
        }
        public void SetData(PlayingField playingField)
        {
            int counter = 0;
            leftGrid.Children.Clear();
            if (playingField.GameType.Equals("Billiard"))
                {
                List<PlayingField> fields = new PlayingFieldDAO().GetPlayingFieldsWithReservation("Billiard");
                fields.ForEach(f =>
                {
                    leftGrid.RowDefinitions.Add(new RowDefinition());
                    f.Image = "pack://application:,,,/GUI/Billiard.png";
                    Control temp = new ElementControl(f);
                    Grid.SetColumn(temp, counter % 2);
                    Grid.SetRow(temp, counter++ / 2);
                    leftGrid.Children.Add(temp);
                });
                }
            else if(playingField.GameType.Equals("Snooker"))
                {
                List<PlayingField> fields = new PlayingFieldDAO().GetPlayingFieldsWithReservation("Snooker");
                fields.ForEach(f =>
                {
                    leftGrid.RowDefinitions.Add(new RowDefinition());
                    f.Image = "pack://application:,,,/GUI/snooker (1).png";
                    Control temp = new ElementControl(f);
                    Grid.SetColumn(temp, counter % 2);
                    Grid.SetRow(temp, counter++ / 2);
                    leftGrid.Children.Add(temp);
                });
            }
            else if (playingField.GameType.Equals("Darts"))
            {
                List<PlayingField> fields = new PlayingFieldDAO().GetPlayingFieldsWithReservation("Darts");
                fields.ForEach(f =>
                {
                    leftGrid.RowDefinitions.Add(new RowDefinition());
                    f.Image = "pack://application:,,,/GUI/pngwave.png";
                    Control temp = new ElementControl(f);
                    Grid.SetColumn(temp, counter % 2);
                    Grid.SetRow(temp, counter++ / 2);
                    leftGrid.Children.Add(temp);
                });
            }
            else if (playingField.GameType.Equals("BlackDarts"))
            {
                List<PlayingField> fields = new PlayingFieldDAO().GetPlayingFieldsWithReservation("BlackDarts");
                fields.ForEach(f =>
                {
                    leftGrid.RowDefinitions.Add(new RowDefinition());
                    f.Image = "pack://application:,,,/GUI/pngwave (1).png";
                    Control temp = new ElementControl(f);
                    Grid.SetColumn(temp, counter % 2);
                    Grid.SetRow(temp, counter++ / 2);
                    leftGrid.Children.Add(temp);
                });
            }
            
        }
    }
}
