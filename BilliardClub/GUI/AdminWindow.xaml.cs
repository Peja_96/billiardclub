﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    
    public partial class AdminWindow : Window
    {
        public static string currentUser;
        public static Admin admin;
        public AdminWindow(string user)
        {
            InitializeComponent();
            currentUser = user;
            admin = new AdminDAO().GetAdmin(user);
            topGrid.Children.Add(new TopMenuControl());
            setLangTheme();
        }

        


        private void listViewMenu_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            
            UserControl usc = null;
            workingGrid.Children.Clear();

            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                case "clubInfo":
                    usc = new ClubInfoControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "finance":
                    usc = new FinanceControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "employees":
                    usc = new EmployeeControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "playingFields":
                    usc = new PlayingFieldsControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "equipment":
                    usc = new EquipmentControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "prices":
                    usc = new PricesControl();
                    workingGrid.Children.Add(usc);
                    break;
                case "yourInfo":
                    usc = new AdminInfoControl();
                    workingGrid.Children.Add(usc);
                    break;
                default:
                    break;
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void signOut_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            new LoginWindow().Show();
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            workingGrid.Children.Add(new ClubInfoControl());
        }

        private void setLangTheme()
        {
            if(admin.Language.Equals("Serbian"))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/SerbianDictionary.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/EnglishDictionary.xaml", UriKind.RelativeOrAbsolute) });
            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/EnglishDictionary.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/SerbianDictionary.xaml", UriKind.RelativeOrAbsolute) });

            }
            if (admin.Theme.Equals("Light"))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });

            }
            else if (admin.Theme.Equals("Dark"))
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });

            }
            else
            {
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Dark.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Add(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/BlackBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme.Light.xaml", UriKind.Absolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/LightBrushes.xaml", UriKind.RelativeOrAbsolute) });
                Application.Current.Resources.MergedDictionaries.Remove(new ResourceDictionary() { Source = new Uri(@"pack://application:,,,/GUI/DarkBrushes.xaml", UriKind.RelativeOrAbsolute) });

            }
        }
    }
}
