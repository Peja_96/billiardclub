﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for PlayingFieldsControl.xaml
    /// </summary>
    public partial class PlayingFieldsControl : UserControl
    {
        public PlayingFieldsControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            PlayingFieldsTable.Items.Clear();
            ImportData();
        }

        private void ImportData()
        {
            List<PlayingField> equipment = new PlayingFieldDAO().GetPlayingFields();
            equipment.ForEach(e => PlayingFieldsTable.Items.Add(e));
        }
        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            var result = new AddPlayingFieldWindow().ShowDialog().Value;
            if (result)
                UserControl_Loaded(sender, null);
        }
        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if (PlayingFieldsTable.SelectedItem != null)
            {
                PlayingField field = (PlayingField)PlayingFieldsTable.SelectedItem;
                new PlayingFieldDAO().RemoveField(field);
                PlayingFieldsTable.Items.Remove(field);
                new CustomMessageBox("Deleted", field.GameType + " " + field.Position + " successfully deleted.", CustomMessageBoxButton.Ok).ShowDialog();
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            PlayingFieldsTable.Items.Clear();
            List<PlayingField> playingFields = new PlayingFieldDAO().GetPlayingFieldsByType(tbxSearch.Text);
            playingFields.ForEach(p => PlayingFieldsTable.Items.Add(p));
        }
        private void EmployeesTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            /* ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
             if (e.VerticalChange > 0)
             {
                 if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                 {
                     offset++;

                     List<Employee> list = new EmployeeDAO().GetTenEmployees(offset * 10);
                     if (list.Count == 0)
                     {
                         offset--;
                         return;
                     }
                     list.ForEach(p => EmployeesTable.Items.Add(p));
                 }
             }*/
        }

        private void PlayingFieldsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
