﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for PricesControl.xaml
    /// </summary>
    public partial class PricesControl : UserControl
    {
        public PricesControl()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Reload();
        }

        private void Reload()
        {
            List<Game> games = new GameDAO().GetGames();
            games.ForEach(g =>
            {
                if (g.Type.Equals("Billiard"))
                    billiardBox.Text = g.PricePerOur.ToString();
                else if (g.Type.Equals("Snooker"))
                    snookerBox.Text = g.PricePerOur.ToString();
                else if (g.Type.Equals("Darts"))
                    dartsBox.Text = g.PricePerOur.ToString();
                else if (g.Type.Equals("BlackDarts"))
                    blackDartsBox.Text = g.PricePerOur.ToString();
            });
        }

        private void changeButton_Click(object sender, RoutedEventArgs e)
        {
            List<Game> games = new GameDAO().GetGames();
            games.ForEach(g =>
            {
                if (g.Type.Equals("Billiard"))
                {
                    g.PricePerOur = Decimal.Parse(billiardBox.Text);
                    new GameDAO().ModifyGamePrice(g);
                }
                else if (g.Type.Equals("Snooker"))
                {
                    g.PricePerOur = Decimal.Parse(snookerBox.Text);
                    new GameDAO().ModifyGamePrice(g);
                }
                else if (g.Type.Equals("Darts"))
                {
                    g.PricePerOur = Decimal.Parse(dartsBox.Text);
                    new GameDAO().ModifyGamePrice(g);
                }
                else if (g.Type.Equals("BlackDarts"))
                {
                    g.PricePerOur = Decimal.Parse(blackDartsBox.Text);
                    new GameDAO().ModifyGamePrice(g);
                }
            });
            Reload();
        }

        private void billiardBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
                
            e.Handled = !IsValidDecimal(((TextBox)sender).Text + e.Text);
        }

        public bool IsValidDecimal(string str)
        {
            decimal i;
            return decimal.TryParse(str, out i) && i > 0;

        }
    }
    
}
