﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BilliardClub.GUI
{
    public enum CustomMessageBoxIcon
    {
        Information,
        Warning,
        Error
    }
}
