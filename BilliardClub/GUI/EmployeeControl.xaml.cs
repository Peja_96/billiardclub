﻿using BilliardClub.DAO;
using BilliardClub.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BilliardClub.GUI
{
    /// <summary>
    /// Interaction logic for EmployeeControl.xaml
    /// </summary>
    public partial class EmployeeControl : UserControl
    {
        public EmployeeControl()
        {
            InitializeComponent();
        }

        private void removeButton_Click(object sender, RoutedEventArgs e)
        {
            if (EmployeesTable.SelectedItem != null)
            {
                Employee employee = (Employee)EmployeesTable.SelectedItem;
                new EmployeeDAO().RemoveEmployee(employee);
                EmployeesTable.Items.Remove(employee);
                new CustomMessageBox("Deleted", "Employee " + employee.Name + " " + employee.Surname + " successfully deleted.", CustomMessageBoxButton.Ok).ShowDialog();
            }
            else
            {
                new CustomMessageBox("Error", "Please select player", CustomMessageBoxButton.Ok, CustomMessageBoxIcon.Error).ShowDialog();
            }
        }

        private void tbxSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            EmployeesTable.Items.Clear();
            List<Employee> list = new EmployeeDAO().GetEmployeesByName(tbxSearch.Text);
            list.ForEach(p => EmployeesTable.Items.Add(p));

        }
        private void EmployeesTable_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
           /* ScrollViewer scrollViewer = e.OriginalSource as ScrollViewer;
            if (e.VerticalChange > 0)
            {
                if (scrollViewer.VerticalOffset == scrollViewer.ScrollableHeight)
                {
                    offset++;

                    List<Employee> list = new EmployeeDAO().GetTenEmployees(offset * 10);
                    if (list.Count == 0)
                    {
                        offset--;
                        return;
                    }
                    list.ForEach(p => EmployeesTable.Items.Add(p));
                }
            }*/
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            bool result = new AddEmoloyeeWindow().ShowDialog().Value;
            if (result)
            {
                UserControl_Loaded(sender, null);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            EmployeesTable.Items.Clear();
            LoadEmployees();
        }

        private void LoadEmployees()
        {
            List<Employee> list = new EmployeeDAO().GetEmployees();
            list.ForEach(e => EmployeesTable.Items.Add(e));
        }
    }
}
